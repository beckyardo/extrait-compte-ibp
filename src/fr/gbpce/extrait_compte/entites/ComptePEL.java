package fr.gbpce.extrait_compte.entites;

public class ComptePEL extends Compte{
	
	private double montantDroitsCreditAccumules;
	
	
	public double getMontantDroitsCreditAccumules() {
		return montantDroitsCreditAccumules;
	}

	public void setMontantDroitsCreditAccumules(double montantDroitsCreditAccumules) {
		this.montantDroitsCreditAccumules = montantDroitsCreditAccumules;
	}
	
	@Override
	public String toString() {
		String resultat = new String("");
		double soldeDebutMois = super.getSolde() + this.montantDroitsCreditAccumules;
		resultat += super.getLibelle() + super.getNumero() + "\nSolde en d�but de mois = " + soldeDebutMois + "\n";
		resultat+=super.toString();
		resultat+= "Vos droits a cr�dit s'elevent a  = " + this.montantDroitsCreditAccumules+"\n";
		resultat+= "Solde en fin de mois = "+ this.calculerSoldeFinMois()+"\n";
		resultat+= "-----------------------------------------------------------";
		return resultat;
	}
	
}

package fr.gbpce.extrait_compte.entites;

import fr.gbpce.extrait_compte.entites.constantes.Constantes;

public class CompteCourant extends Compte {
	
	private String numeroContratCarteBancaireAssocie;

	public String getNumeroContratCarteBancaireAssocie() {
		return numeroContratCarteBancaireAssocie;
	}

	public void setNumeroContratCarteBancaireAssocie(String numeroContratCarteBancaireAssocie) {
		this.numeroContratCarteBancaireAssocie = numeroContratCarteBancaireAssocie;
	}

	@Override
	public String toString() {
		String resultat = new String("");
		double soldeDebutMois = super.getSolde() - (super.getSolde() * Constantes.POURCENTAGE_FRAIS_TENUE_COMPTE_COURANT);
		resultat += super.getLibelle() + super.getNumero() + "\nSolde en d�but de mois = " + soldeDebutMois + "\n";
		resultat+=super.toString()+"\n";
		resultat+= "Solde en fin de mois = "+ this.calculerSoldeFinMois()+ "\n";
		resultat+= "-----------------------------------------------------------";
		return resultat;
	}
	

}

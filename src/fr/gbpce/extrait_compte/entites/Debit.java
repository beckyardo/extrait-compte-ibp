package fr.gbpce.extrait_compte.entites;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import fr.gbpce.extrait_compte.entites.constantes.Constantes;

public class Debit extends Operation{
	DateFormat dateFormat = new SimpleDateFormat(Constantes.FORMAT_DATE_DDMMYYYY);
	
	@Override
	public String toString() {
		return dateFormat.format(this.getDate())+",D�bit," + this.getMontant()+" "+ Constantes.DEVISE+","+ this.getLibelle() +","+this.getCategorie()+"\n";
	}

}

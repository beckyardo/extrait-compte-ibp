package fr.gbpce.extrait_compte.entites.constantes;

public class Constantes {
	
	public static String SEPARATEUR = ";";
	public static String DEVISE = "euros";
	public static String FORMAT_DATE_DDMMYYYY = "dd/MM/yyyy";
	public static double POURCENTAGE_FRAIS_TENUE_COMPTE_COURANT = 0.005;
	public static double POURCENTAGE_CREDIT_CUMULE = 0.002;
	public static String CHEMIN_FICHIER_COMPTE_COURANT = "C:/EcritureComptableCPT34319312576.csv";
	public static String CHEMIN_FICHIER_COMPTE_PEL = "C:/EcritureComptablePEL94461497433.csv";

}

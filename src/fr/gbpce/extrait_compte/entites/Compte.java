package fr.gbpce.extrait_compte.entites;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import fr.gbpce.extrait_compte.entites.constantes.TypeOperation;

public abstract class Compte {

	private String numero;
	private String libelle;
	private double solde;
	private List<Operation> operations;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	@Override
	public String toString() {
		String resultat = new String("");
		resultat += listeEcrituresTriees();
		return resultat;
	}

	private String listeEcrituresTriees() {
		String resultat = new String("");
		List<Operation> operationsTriees = this
				.getOperations().stream().sorted(Comparator.comparing(Operation::getDate)
						.thenComparing(Operation::getType).thenComparing(Operation::getMontant))
				.collect(Collectors.toList());
		for (Operation operation : operationsTriees) {
			resultat += operation.toString();
		}
		return resultat;
	}

	public double calculerSoldeFinMois() {
		double solde = this.solde;
		for (Operation operation : this.operations) {
			if (operation.getType().equals(TypeOperation.C.name())) {
				solde = solde + operation.getMontant();
			} else {
				solde = solde - operation.getMontant();
			}
		}
		return solde;
	}

}

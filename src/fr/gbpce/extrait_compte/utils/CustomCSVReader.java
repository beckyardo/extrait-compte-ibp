package fr.gbpce.extrait_compte.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystemNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import fr.gbpce.extrait_compte.entites.Credit;
import fr.gbpce.extrait_compte.entites.Debit;
import fr.gbpce.extrait_compte.entites.Operation;
import fr.gbpce.extrait_compte.entites.constantes.Constantes;
import fr.gbpce.extrait_compte.entites.constantes.TypeOperation;

public class CustomCSVReader {

	public static List<Operation> lireFichier(String inputFilePath) {
		DateFormat dateFormat = new SimpleDateFormat(Constantes.FORMAT_DATE_DDMMYYYY);
		List<Operation> inputList = new ArrayList<Operation>();

		try {

			File inputF = new File(inputFilePath);
			InputStream inputFS = new FileInputStream(inputF);
			BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
			inputList = br.lines().skip(1).map(line -> {
				String[] p = line.split(Constantes.SEPARATEUR);
				Operation item;
				if (p[1].equals(TypeOperation.C.toString())) {
					item = new Credit();
				} else {
					item = new Debit();
				}
				Date date;
				try {
					date = dateFormat.parse(p[0]);
					item.setDate(date);
					item.setType(p[1]);
					item.setMontant(Double.parseDouble(p[2]));
					item.setLibelle(p[3]);
					if (p.length > 4) {
						item.setCategorie(p[4]);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return item;
			}).collect(Collectors.toList());
			br.close();
		} catch (FileSystemNotFoundException | IOException e) {
		}

		return inputList;
	}

}

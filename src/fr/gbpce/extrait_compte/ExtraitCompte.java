package fr.gbpce.extrait_compte;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.gbpce.extrait_compte.entites.Compte;
import fr.gbpce.extrait_compte.entites.CompteCourant;
import fr.gbpce.extrait_compte.entites.ComptePEL;
import fr.gbpce.extrait_compte.entites.Operation;
import fr.gbpce.extrait_compte.entites.constantes.Constantes;
import fr.gbpce.extrait_compte.entites.constantes.TypeOperation;
import fr.gbpce.extrait_compte.utils.CustomCSVReader;

public class ExtraitCompte {

	public static void main(String[] args) {
		// Chargement des donn�es des fichiers CVS
		List<Operation> operationsCompteCourant = CustomCSVReader.lireFichier(Constantes.CHEMIN_FICHIER_COMPTE_COURANT);
		List<Operation> operationsComptePEL = CustomCSVReader.lireFichier(Constantes.CHEMIN_FICHIER_COMPTE_PEL);
		List<Compte> listeComptes = new ArrayList<Compte>();

		CompteCourant courant = new CompteCourant();
		courant.setLibelle("compte Mr Dupont");
		courant.setNumero("CPT34319312576");
		courant.setSolde(1487.8);
		courant.setNumeroContratCarteBancaireAssocie("GFC0144312576");
		courant.setOperations(operationsCompteCourant);
		listeComptes.add(courant);

		ComptePEL pel = new ComptePEL();
		pel.setLibelle("Pel Charles D .");
		pel.setNumero("PEL94461497433");
		pel.setSolde(4400);
		pel.setMontantDroitsCreditAccumules(250);
		pel.setOperations(operationsComptePEL);
		listeComptes.add(pel);
		
		for(Compte compte : listeComptes) {
			System.out.println(compte);
		}
		
		System.out.println("Frais bancaire du prochain mois :");
		System.out.println("Les frais pour le compte courant "+ courant.getNumero() + " seront de "+ courant.calculerSoldeFinMois() * Constantes.POURCENTAGE_FRAIS_TENUE_COMPTE_COURANT+" "+Constantes.DEVISE);
		System.out.println("Les frais pour le compte sur livret "+ pel.getNumero() + " seront de "+ 0 +" "+Constantes.DEVISE);
		System.out.println("-----------------------------------------------------------");
		System.out.println("Analyse des d�penses du compte "+courant.getNumero());
		
		//Regroupement par cat�gories des op�rations de D�bit
		Map<String,List<Operation>> mapByCategories = courant.getOperations().stream().filter(op-> op.getCategorie()!=null && op.getType().equals(TypeOperation.D.name()))
        .collect(Collectors.groupingBy(Operation::getCategorie));
		
		for (Map.Entry<String, List<Operation>> entry : mapByCategories.entrySet()) {
	          System.out.println(entry.getKey() + ",nombre d'op�rations : " + entry.getValue().size() +",Montant total : "+ entry.getValue().stream().mapToDouble(Operation::getMontant).sum()+ " "+Constantes.DEVISE);
	      }

		
	}
}
